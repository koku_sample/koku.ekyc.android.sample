package com.koku.ekycapp

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.koku.ekycsdk.KoKuKYCSDK
import com.koku.ekycsdk.model.KYCError
import com.koku.ekycsdk.model.KYCResult
import com.koku.ekycsdk.utils.APIEnvironemnt
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
    private lateinit var kycSdk: KoKuKYCSDK

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        callMultiplePermissions()

        button.setOnClickListener {
            moveToeKYC()
        }
    }

    fun moveToeKYC() {

        val referenceId = UUID.randomUUID().toString()

        /**
         * prepare KoKuKYCSDK instance here as below
         *
         */
        kycSdk = KoKuKYCSDK.Builder(application)
            .setCountryCode("ID")
            .setCallBackUrl("") // this url should be a web hook url for eKYC server to invoked your service with the KYC result.
            .setReferenceId(referenceId)
            .init(BuildConfig.CLIENT_ID, BuildConfig.SECRET_KEY, BuildConfig.SDK_SUBSCRIPTION)
            .setAPIEnvironment(APIEnvironemnt.STAGING)
            .build()

        /**
         * startKycActivity here to start the KYC process.
         *
         */

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                kycSdk.startKycActivity(
                    this,
                    referenceId
                )
            } else {
                AlertDialog.Builder(this)
                    .setMessage("CAMERA permission is required in order to verify your KYC information. We use camera to take photo of user's ID card or verify facial recognition of the user. Please kindly allow the permissions to proceed.")
                    .setPositiveButton("Ok") { _ , _ ->
                        callMultiplePermissions()
                    }
                    .create()
                    .show()
            }
        } else {
            kycSdk.startKycActivity(
                this,
                referenceId
            )
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        /**
         * handle KYC Result here
         */
        if (kycSdk != null) {
            kycSdk.handleActivityResult(requestCode, resultCode, data, object : KoKuKYCSDK.Callbacks {
                override fun onKYCError(kycError: KYCError?) {
                    if (kycError != null) {
                        Log.e("Error", kycError.message)
                        Toast.makeText(this@MainActivity, kycError.message, Toast.LENGTH_SHORT).show()
                    }

                }

                override fun onKYCSuccess(kycResult: KYCResult) {
                    Log.d("Final KYC Result", kycResult.toString())
                    Toast.makeText(
                        this@MainActivity,
                        "Success kyc for: ${kycResult.ocrResponse!!.ocrInfo.name}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        } else {
            // Pre-Marshmallow
        }

        return true
    }

    private fun callMultiplePermissions() {
        val permissionsNeeded = ArrayList<String>()

        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("NETWORK STATE")
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE")
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA")

        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) {
                // Need Rationale
                var message = "You need to grant access to " + permissionsNeeded[0]
                for (i in 1 until permissionsNeeded.size)
                    message = message + ", " + permissionsNeeded[i]

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(
                        permissionsList.toTypedArray(),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                    )
                }

                return
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(
                    permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                )
            }
            return
        }

    }
}
